class Mailer:
    subjects = {
        'confirm_email': 'Активация аккаунта',
        'forgot_password': 'Восстановление пароля'
    }

    templates = {
        'confirm_email': 'confirm_email.html',
        'forgot_password': 'forgot_password.html'
    }

