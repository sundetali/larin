from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from back.apps.users.models import User


def send_mail(domain: str, user: User, template: str, subject: str):
    message = render_to_string(template, {
        'user': user,
        'domain': domain,
        'confirm_key': user.confirm_key
    })
    email = EmailMessage(subject, message, to=[user.email])
    email.content_subtype = 'html'
    email.send()

