from django.urls import path

from back.apps.authorization.views import (UserRegistrationView,
                                           UserConfirmEmailView,
                                           UserLoginView,
                                           UserForgotPasswordView,
                                           UserSetPasswordView
                                           )

app_name = 'auth'

urlpatterns = [
    path('register/', UserRegistrationView.as_view(), name='register'),
    path('confirm-email/', UserConfirmEmailView.as_view(), name='confirm-email'),
    path('login/', UserLoginView.as_view(), name='login'),
    path('forgot-password/', UserForgotPasswordView.as_view(), name="forgot_password"),
    path('set-password/', UserSetPasswordView.as_view(), name="set_password"),
]
