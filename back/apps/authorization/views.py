import random

from django.contrib.sites.shortcuts import get_current_site
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from back.apps.authorization.tasks import send_mail
from back.apps.users.models import User
from back.apps.users.serializers import UserRetrieveSerializer
from back.apps.authorization.serializers import (UserRegistrationSerializer,
                                            UserLoginSerializer,
                                            UserSetPasswordSerializer,
                                            UserForgotPasswordSerializer
                                            )
from back.apps.authorization import Mailer


class UserRegistrationView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserRegistrationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"ref": request.query_params.get("ref")})
        serializer.is_valid(raise_exception=True)
        user = self.perform_create(serializer)

        template = Mailer.templates['confirm_email']
        subject = Mailer.subjects['confirm_email']
        domain = get_current_site(request).domain

        send_mail(domain, user, template, subject)

        return Response(UserRetrieveSerializer(user).data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()


class UserConfirmEmailView(APIView):
    permission_classes = (AllowAny, )

    def get(self, request):
        email = request.query_params['email']
        confirm_key = request.query_params['confirm_key']

        user = User.objects.filter(email=email, confirm_key=confirm_key).first()
        if user:
            user.email_confirmed = True
            user.save()
            return Response({'status': 'success'}, status=status.HTTP_200_OK)

        return Response({'status': 'failed'}, status=status.HTTP_404_NOT_FOUND)


class UserLoginView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(data=serializer.validated_data, status=status.HTTP_200_OK)


class UserForgotPasswordView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserForgotPasswordSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data['email']

        user = User.objects.filter(email=email).first()

        if user:
            user.recover_code = random.randint(100000, 999999)
            user.save()

            domain = get_current_site(request).domain
            template = Mailer.templates['forgot_password']
            subject = Mailer.subjects['forgot_password']
            send_mail(domain, user, template, subject)

            return Response({'status': 'success'}, status=status.HTTP_200_OK)

        return Response({'status': 'failed'}, status=status.HTTP_404_NOT_FOUND)


class UserSetPasswordView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserSetPasswordSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        assert serializer.is_valid(raise_exception=True)

        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK,
        )