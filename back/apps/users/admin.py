from django.contrib import admin

from back.apps.users.models import User

admin.site.register(User)
