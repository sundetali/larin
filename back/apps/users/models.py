from datetime import datetime, timedelta

import jwt
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models
from django.conf import settings

from back.apps.users.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=150, unique=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150, null=True, blank=True)
    iin = models.CharField(max_length=12, validators=[RegexValidator(r'^[0-9]{12}$')])
    phone_number = models.CharField(
        max_length=11,
        validators=[RegexValidator(regex=r'^(7)([0-9]{10})$',
                                   message="Phone number must be entered in the format: '77071230077'. Up to 11 digits allowed.")])
    birth_date = models.DateField(null=True, blank=True)
    confirm_key = models.CharField(max_length=200)
    email_confirmed = models.BooleanField(default=False)
    recover_code = models.IntegerField(null=True, blank=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    ref = models.CharField(max_length=32, null=True, blank=True)
    parent = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True, related_name="sub_users")
    objects = UserManager()

    USERNAME_FIELD = "email"

    def __str__(self):
        return self.email

    @property
    def token(self):
        return self._generate_jwt_token()

    def _generate_jwt_token(self):
        dt = datetime.now() + timedelta(days=3)
        token = jwt.encode({
            'id': self.id,
            'exp': int(dt.strftime('%s'))
        }, settings.SECRET_KEY, algorithm='HS256')

        return token

