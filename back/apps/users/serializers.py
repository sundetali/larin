from django.urls import reverse
from rest_framework import serializers

from back.apps.users.models import User


class ParentUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email']


class SubUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email']


class UserRetrieveSerializer(serializers.ModelSerializer):
    ref_link = serializers.SerializerMethodField()
    sub_users = SubUserSerializer(many=True)
    parent = ParentUserSerializer()

    def get_ref_link(self, obj):
        request = self.context.get('request')
        if request and obj.ref:
            query = f"?ref={obj.ref}"
            return f"{request.get_host()}{reverse('auth:register')}{query}"

    class Meta:
        model = User
        fields = ('id', 'email', 'phone_number', 'iin', 'first_name', 'last_name', 'ref_link', 'parent', 'sub_users')
