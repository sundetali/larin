from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.crypto import get_random_string

from back.apps.users.models import User


@receiver(post_save, sender=User)
def generate_ref(sender, instance, created, **kwargs):
    if instance.is_active and not instance.ref:
        instance.ref = get_random_string(length=32)
        instance.save()
