from django.urls import path

from back.apps.users.views import UserRetrieveView

app_name = 'user'

urlpatterns = [
    path('me/', UserRetrieveView.as_view(), name='me')
]
