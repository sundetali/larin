from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from back.apps.users.serializers import UserRetrieveSerializer


class UserRetrieveView(RetrieveAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = UserRetrieveSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = request.user
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


