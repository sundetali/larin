from django.urls import path

from front.main import views
app_name = "main"

urlpatterns = [
    path("signup/", views.signup, name="signup"),
    path("login/", views.login, name="login"),
]