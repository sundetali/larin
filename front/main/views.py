from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt


def generic(request):
    return render(request, "generic.html")


def elements(request):
    return render(request, "elements.html")


def login(request):
    return render(request, "login.html")


@csrf_exempt
def signup(request):
    return render(request, "signup.html")
