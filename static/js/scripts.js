(function($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
        $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
            if (this.href === path) {
                $(this).addClass("active");
            }
        });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });
})(jQuery);

const BASE_URL = 'http://0.0.0.0:8000/api/authorization'

$(document).ready(function () {
    $('.save-create-changes-button').on('click', function(e){
        let email = $('#email').val()
        let iin = $('#iin').val()
        let first_name = $('#first_name').val()
        let last_name = $('#last_name').val()
        let birth_date = $('#birth_date').val()
        let phone_number = $('#phone_number').val()
        let password = $('#password').val()

        $.ajax({
          type: "POST",
          url:`${ BASE_URL }/register/`,
          data: {
              email,
              iin,
              first_name,
              last_name,
              birth_date,
              phone_number,
              password
          },
          success: function(){
              console.log('success')
              alert('success');
          },
          error: function(){
             alert('failure');
          }
        });
    });
})

